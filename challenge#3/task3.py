import json
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--object', required=True)
    parser.add_argument('-k', '--key', required=True)
    args = parser.parse_args()
    d = args.object
    sys_obj = json.loads(json.dumps(d))
    key = args.key.split("/")
    for i in range(len(key)):
        sys_obj = sys_obj[key[i]]
    print("value is: ",sys_obj)

main()